//
//  main.m
//  GridGame
//
//  Created by Julietta Yaunches on 3/31/14.
//  Copyright (c) 2014 Julietta Yaunches. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
