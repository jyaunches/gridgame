//
//  AppDelegate.h
//  GridGame
//
//  Created by Julietta Yaunches on 3/31/14.
//  Copyright (c) 2014 Julietta Yaunches. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
